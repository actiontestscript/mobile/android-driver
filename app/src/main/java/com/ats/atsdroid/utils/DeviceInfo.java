/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.atsdroid.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Insets;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.*;
import androidx.test.platform.app.InstrumentationRegistry;
import com.ats.atsdroid.BuildConfig;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;

public class DeviceInfo {
    
    //----------------------------------------------------------------------------------
    // Singleton declaration
    //----------------------------------------------------------------------------------

    private static DeviceInfo instance;
    
    public static DeviceInfo getInstance() {
        if (instance == null) {
            instance  = new DeviceInfo();
        }
        
        return instance;
    }

    private Context getContext() {
        return InstrumentationRegistry.getInstrumentation().getContext();
    }
    
    private DeviceInfo() {
        AtsAutomation.sendLogs("Get bluetooth adapter name\n");
        
        hostName = tryGetHostname();
        systemName = getAndroidVersion();
    }

    //----------------------------------------------------------------------------------
    // Utils
    //----------------------------------------------------------------------------------

    private String getAndroidVersion() {
        double release = Double.parseDouble(Build.VERSION.RELEASE.replaceAll("(\\d+[.]\\d+)(.*)","$1"));
        
        String codeName = "Android" + (int)release;
        if (release < 6) codeName = "Lollipop";
        else if (release < 7) codeName = "Marshmallow";
        else if (release < 8) codeName = "Nougat";
        else if (release < 9) codeName = "Oreo";
        else if (release < 10) codeName = "Pie";
        
        return codeName + " (" + release + "), API Level: " + Build.VERSION.SDK_INT;
    }

    public static String tryGetHostname() {
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        AtsAutomation.sendLogs("getting IP address" + inetAddress.getHostName() + "\n");
                        return inetAddress.getHostName();
                    }
                }
            }
        } catch (SocketException e) {
            return "error -> " + e;
        }
        return "undefined";
    }
    
    //----------------------------------------------------------------------------------
    // Instance access
    //----------------------------------------------------------------------------------
    
    enum PropertyName {
        // airplaneModeEnabled,
        // nightModeEnabled,
        wifiEnabled,
        bluetoothEnabled,
        lockOrientationEnabled,
        orientation,
        // brightness,
        volume;
    
        public static String[] getNames() {
            PropertyName[] states = values();
            String[] names = new String[states.length];
    
            for (int i = 0; i < states.length; i++) {
                names[i] = states[i].name();
            }
    
            return names;
        }
    }
    
    private int port;
    private int deviceWidth;
    private int deviceHeight;
    private int channelWidth;
    private int channelHeight;
    
    private int navBarInset;
    // private int displayCutoutInset = 0;
    
    private float scale;
    
    private Matrix matrix;

    private final String systemName;
    private final String deviceId = Build.ID;
    private final String model = Build.MODEL;
    private final String manufacturer = StringUtils.capitalize(Build.MANUFACTURER);
    private final String brand = StringUtils.capitalize(Build.BRAND);
    private final String deviceName = Settings.Global.getString(getContext().getContentResolver(), Settings.Global.DEVICE_NAME);
    private final String version = Build.VERSION.RELEASE;
    private final String build = Build.VERSION.INCREMENTAL;
    private String hostName;
    private String serialNumber;

    public void initDevice(int p, String ipAddress, String serial) {
        serialNumber = serial;
        hostName = ipAddress;
        port = p;
    }
    
    static public int getScreenRotation() {
        final Display display = ((WindowManager) InstrumentationRegistry.getInstrumentation().getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        if (display == null) {
            return Surface.ROTATION_0;
        } else {
            return display.getRotation();
        }
    }

    public void setupScreenInformation() {
        final WindowManager manager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Point point = new Point();
        
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
            final WindowMetrics windowsMetrics = manager.getCurrentWindowMetrics();
            
            // Gets all excluding insets
            final WindowInsets windowInsets = windowsMetrics.getWindowInsets();
            final Insets navBarInsets = windowInsets.getInsetsIgnoringVisibility(WindowInsets.Type.navigationBars());
            // final Insets displayCutoutInsets = windowInsets.getInsetsIgnoringVisibility(WindowInsets.Type.displayCutout());
    
            navBarInset = Collections.max(Arrays.asList(navBarInsets.bottom, navBarInsets.right, navBarInsets.left, navBarInsets.top));
            // displayCutoutInset = Collections.max(Arrays.asList(displayCutoutInsets.bottom, displayCutoutInsets.right, displayCutoutInsets.left, displayCutoutInsets.top));
            
            final Rect bounds = windowsMetrics.getBounds();
            point = new Point(bounds.width(), bounds.height());
        } else {
            navBarInset = navBarHeight();
            
            manager.getDefaultDisplay().getRealSize(point);
        }
    
        channelWidth = point.x;
        channelHeight = point.y;
    
        final DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        scale = displayMetrics.scaledDensity;
        
        deviceWidth = (int) (channelWidth / scale);
        deviceHeight = (int) (channelHeight / scale);
        
        matrix = new Matrix();
        matrix.preScale(1 /scale, 1 / scale);
    }
    
    private int navBarHeight() {
        final Resources resources = getContext().getResources();
        final int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }
    
    public void driverInfoBase(JSONObject obj) throws JSONException {
        setupScreenInformation();
        
        obj.put("os", "android");
        obj.put("driverVersion", BuildConfig.VERSION_NAME);
        obj.put("systemName", systemName);
        obj.put("deviceRotation", getScreenRotation());
        obj.put("deviceWidth", deviceWidth);
        obj.put("deviceHeight", deviceHeight);
        obj.put("deviceHeightWithoutNavBar", (int) ((channelHeight - navBarInset) / scale));
        obj.put("channelWidth", channelWidth);
        obj.put("channelHeight", channelHeight);
        obj.put("channelHeightWithoutNavBar", channelHeight - navBarInset);
        obj.put("scale", scale);
        obj.put("navBarInset", navBarInset);
        obj.put("osBuild", build);
        obj.put("mobileUser", "");
        obj.put("deviceName", deviceName);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            obj.put("country", getContext().getResources().getConfiguration().getLocales().get(0).getCountry());
        } else {
            obj.put("country", getContext().getResources().getConfiguration().locale.getCountry());
        }

        obj.put("systemProperties", new JSONArray(PropertyName.getNames()));
        obj.put("systemButtons", new JSONArray(SysButton.ButtonType.getNames()));
        obj.put("serialNumber", serialNumber);
        obj.put("statusPage", hostName + ":" + port + "/status");
        obj.put("chromeDriver", "");
        obj.put("chromeVersion", "0");
        obj.put("chromeDriverVersion", "0");
    }
    
    public String getSystemName() {
        return systemName;
    }
    
    public String getDeviceId() {
        return deviceId;
    }
    
    public String getDeviceName() {
        return deviceName;
    }
    
    public String getModel() {
        return model;
    }
    
    public String getManufacturer() {
        return manufacturer;
    }
    
    public String getBrand() {
        return brand;
    }
    
    public String getVersion() {
        return version;
    }
    
    public String getHostName() {
        return hostName;
    }
    
    public int getPort() {
        return port;
    }
    
    public int getDeviceWidth() {
        return deviceWidth;
    }
    
    public int getDeviceHeight() {
        return deviceHeight;
    }
    
    public int getChannelWidth() {
        return channelWidth;
    }
    
    public int getChannelHeight() {
        return channelHeight;
    }
    
    public int getNavBarInset() {
        return navBarInset;
    }
    
    public Matrix getMatrix(){
        return matrix;
    }
}