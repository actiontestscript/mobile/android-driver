package com.ats.atsdroid.ui;

import android.content.Context;
import android.graphics.*;
import android.view.View;
import com.ats.atsdroid.BuildConfig;
import com.ats.atsdroid.R;
import com.ats.atsdroid.utils.DeviceInfo;

public class AtsView extends View {
	
	private Paint backGroundPaint;
	private final Paint textPaint;
	private final Bitmap bitmap;
	private final int scaledSize;
	
	private Rect rectangle;
	
	public AtsView(Context context) {
		super(context);
		
		scaledSize = getResources().getDimensionPixelSize(R.dimen.font_size);
		
		textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		textPaint.setColor(Color.WHITE);
		textPaint.setTextSize(scaledSize);
		textPaint.setShadowLayer(24.0f, 6.0f, 6.0f, Color.rgb(0, 0, 0));
		
		bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ats_logo2);
	}
	
	private Paint getBackGroundPaint(int w, int h) {
		if (backGroundPaint == null) {
			backGroundPaint = new Paint();
			backGroundPaint.setShader(new LinearGradient(0, 0, w, h, Color.rgb(60, 60, 60), Color.rgb(20, 20, 20), Shader.TileMode.CLAMP));
			backGroundPaint.setAlpha(220);
		}
		return backGroundPaint;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		if (rectangle == null) {
			int h = bitmap.getScaledHeight(canvas);
			int w = bitmap.getScaledWidth(canvas);
			rectangle = new Rect(0, 0, w + 80, h + (scaledSize * 12));
			setLeft((getWidth() - rectangle.width()) / 2);
			setTop((getHeight() - rectangle.height()) / 2);
		}
		
		DeviceInfo deviceInfo = DeviceInfo.getInstance();
		
		canvas.drawRect(rectangle, getBackGroundPaint(rectangle.width(), rectangle.height()));
		canvas.drawBitmap(bitmap, 0, 0, null);
		
		int yPos = scaledSize * 6;
		canvas.drawText("ATS driver host : " + deviceInfo.getHostName() + ":" + deviceInfo.getPort(), 30, yPos, textPaint);
		yPos += scaledSize * 1.4;
		canvas.drawText("ATS driver version : " + BuildConfig.VERSION_NAME, 30, yPos, textPaint);
		yPos += scaledSize * 1.8;
		canvas.drawText("System name : " + deviceInfo.getSystemName(), 30, yPos, textPaint);
		yPos += scaledSize * 1.4;
		canvas.drawText("Channel size : " + deviceInfo.getChannelWidth() + " x " + deviceInfo.getChannelHeight(), 30, yPos, textPaint);
		yPos += scaledSize * 1.8;
		canvas.drawText("Device size : " + deviceInfo.getDeviceWidth() + " x " + deviceInfo.getDeviceHeight(), 30, yPos, textPaint);
		yPos += scaledSize * 1.4;
		canvas.drawText("Device name : " + deviceInfo.getManufacturer() + " " + deviceInfo.getModel(), 30, yPos, textPaint);
	}
}